import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import AuthPageComponent from "./components/AuthPageComponent/AuthPageComponent";
import RatingPageComponent from "./components/RatingPageComponent/RatingPageComponent";
import GamePageComponent from "./components/GamePageComponent/GamePageComponent";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path={"/"}
          element={<AuthPageComponent />}
        ></Route>
        <Route
          path={"/rating"}
          element={<RatingPageComponent />}
        ></Route>
        <Route
          path={"/game"}
          element={<GamePageComponent />}
        ></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
