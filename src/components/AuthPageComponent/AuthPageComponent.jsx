import dogImage from "./dog.png";
import "./AuthPageComponent.scss";
import { useState } from "react";
export default function AuthPageComponent() {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  function handleKeyDown(e) {
    // Не даем пользователю ввести символы кроме: английские буквы, цифры и знаки: точка, нижнее подчеркивание
    const charCode = e.key.charCodeAt(0);

    if (
      charCode !== 46 && // .
      charCode !== 95 && // _
      !(charCode >= 48 && charCode <= 57) && // 0-9
      !(charCode >= 65 && charCode <= 90) && // A-Z
      !(charCode >= 97 && charCode <= 122) // a-z
    )
      e.preventDefault();
  }

  function handleSubmit(e) {
    console.log(`Логин: ${login}, пароль: ${password}`);
    e.preventDefault();
  }

  return (
    <div className={"auth-form-container"}>
      <form className={"auth-form"} onSubmit={(e) => handleSubmit(e)}>
        <img
          src={dogImage}
          alt={"Изображение мультяшной собаки."}
          className={"auth-form__dog-img"}
        />
        <h1 className={"auth-form__headline"}>Войдите в игру</h1>
        <input
          className={"auth-form__input"}
          type={"text"}
          placeholder={"Логин"}
          value={login}
          onChange={(e) => setLogin(e.target.value)}
          onCopy={(e) => e.preventDefault()}
          onKeyDown={(e) => handleKeyDown(e)}
        />
        <input
          className={"auth-form__input"}
          type={"password"}
          placeholder={"Пароль"}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          onCopy={(e) => e.preventDefault()}
          onKeyDown={(e) => handleKeyDown(e)}
        />
        <button className={`auth-form__login-button ${login && password && 'auth-form__login-button--active'}`} type={"submit"}>
          Войти
        </button>
      </form>
    </div>
  );
}
